#!/bin/bash

# Formatting shortcuts
bold=$(tput bold)
normal=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)


# Generate a configuration file using the bean generator python script
python ./tools/beans_generator/beans_generator.py
if [[ ! $? -eq 0 ]]
then
    echo
	echo "${bold}${red}Error: beans_generator.py failed. Aborting!${normal}"
	exit 1
fi

bean_source_path="./tools/beans_generator/crawler-beans.cxml"

# Create the job directory with a user-specified name
echo "Enter a new job name:"
read dirname

if [ ! -d "./jobs/$dirname" ]
then
    echo "Job doesn't exist. Creating now..."
    mkdir ./jobs/$dirname
else
    echo "Job already exists. Clearing..."
    rm -r ./jobs/$dirname
    mkdir ./jobs/$dirname
fi
touch ./jobs/$dirname/crawler-beans.cxml
echo "${green}Done.${normal}"

# Compile Heritrx
echo "Compiling heritrix, please wait..."
mvn package -q
if [[ $? -eq 0 ]]
then
    echo "${green}Done.${normal}"
else
    echo
	echo "${bold}${red}Error: Heritrix was not compiled successfully. Aborting!${normal}"
	exit 1
fi

# Move the compiled jar to the lib folder and start heritrix
mv target/heritrix.willump-1.0-SNAPSHOT.jar lib/
chmod +x lib/heritrix.willump-1.0-SNAPSHOT.jar
echo "" > ./heritrix_out.log
./src/hertrix -a admin:admin
if [[ ! $? -eq 0 ]]
then
    echo
	echo "${bold}${red}Error: Heritrix could not be started!${normal}"
	exit 1
fi

# Copy the generated crawler beans file to the job directory
cp $bean_source_path ./jobs/$dirname/crawler-beans.cxml

echo
echo "#####################"
echo "## WILLUMP STARTED ##"
echo "#####################"
echo "${bold}Heritrix is listening on https://localhost:8443${normal}"
echo "Press Ctrl+C to shut down the heritrix instance."
trap ctrl_c INT

# Function called when Ctrl+C is pressed to kill the heritrix process
function ctrl_c() {
	echo
	echo "Shutting down..."
	pid=`cat heritrix.pid`
	echo "Killing heritrix process id $pid"
	kill $pid
	exit
}

# Sleep to keep the script in the foreground
while true; do
   sleep 1
done
