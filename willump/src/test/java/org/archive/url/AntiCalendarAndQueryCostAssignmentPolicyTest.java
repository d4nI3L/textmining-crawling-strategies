package org.archive.url;

import org.apache.commons.httpclient.URIException;
import org.archive.crawler.frontier.AntiCalendarAndQueryCostAssignmentPolicy;
import org.archive.modules.CrawlURI;
import org.archive.net.UURI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AntiCalendarAndQueryCostAssignmentPolicyTest extends AntiCalendarAndQueryCostAssignmentPolicy {

    private CrawlURI domainTestUri;
    private CrawlURI domainTestUriwithLogin;
    private CrawlURI domainTestUriwithQuery;
    private CrawlURI domainTestUriwithImg;

    @Before
    public void setup() {
        try {
            UsableURI u1 = new UsableURI("https://focus.de/tests/123", true);
            UsableURI u2 = new UsableURI("https://focus.de/tests", true);
            UURI uUri = new UURI(u1, u2);
            domainTestUri = new CrawlURI(uUri);
            UsableURI u3 = new UsableURI("https://focus.de/tests", true);
            UsableURI u4 = new UsableURI("https://focus.de/tests/login", true);
            UURI uUri1 = new UURI(u3, u4);
            domainTestUriwithLogin = new CrawlURI(uUri1);
            UsableURI u5 = new UsableURI("https://focus.de/tests", true);
            UsableURI u6 = new UsableURI("https://focus.de/tests/?query", true);
            UURI uUri2 = new UURI(u5, u6);
            domainTestUriwithQuery = new CrawlURI(uUri2);
            UsableURI u7 = new UsableURI("https://focus.de/tests", true);
            UsableURI u8 = new UsableURI("https://focus.de/tests/test.img", true);
            UURI uUri3 = new UURI(u7, u8);
            domainTestUriwithImg = new CrawlURI(uUri3);
        } catch (URIException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void testCostOf() throws URIException {

        Assert.assertEquals("Expected Cost ", 1, costOf(domainTestUri));

        Assert.assertEquals("Expected Cost ", 11, costOf(domainTestUriwithLogin));

        Assert.assertEquals("Expected Cost ", 2, costOf(domainTestUriwithQuery));

        Assert.assertEquals("Expected Cost ", 101, costOf(domainTestUriwithImg));


    }


}
