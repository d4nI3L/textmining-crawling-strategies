package org.archive.url;


import org.archive.modules.CrawlURI;
import org.archive.modules.fetcher.FetchSolr;
import org.junit.Assert;
import org.junit.Test;


public class FetchSolrTest extends FetchSolr {
    public FetchSolr testFetch = new FetchSolr();
    private CrawlURI domainTestUri;

    @Test
    public void testEscapeQueryUri() {
        String uriTest = "https://focus.de/tests/123";
        Assert.assertEquals("Expected domain: ", "https\\://focus.de/tests/123", escapeQueryUri(uriTest));
        uriTest = "";
        Assert.assertEquals("Expected domain: ", "", escapeQueryUri(uriTest));
        uriTest = "https://focus.de/tests/123=(123)|(1234)";
        Assert.assertEquals("Expected domain: ", "https\\://focus.de/tests/123=\\(123\\)\\|\\(1234\\)", escapeQueryUri(uriTest));

    }


    @Test
    public void shouldProcess() {
        Assert.assertEquals("Expected true ", true, shouldProcess(domainTestUri));
    }


}
