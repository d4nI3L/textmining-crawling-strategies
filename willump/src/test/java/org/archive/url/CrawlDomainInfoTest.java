package org.archive.url;

import org.apache.commons.httpclient.URIException;
import org.archive.crawler.postprocessor.CrawlDomainInfo;
import org.archive.modules.CrawlURI;
import org.archive.net.UURI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;

public class CrawlDomainInfoTest extends CrawlDomainInfo {
    private CrawlURI domainTestUri;


    @Before
    public void setup() {
        add("https://focus.de/tests/test1", 1000);
        add("https://focus.de/tests/test2", 1234);
        add("https://focus.de/tests/test3", 1010);
        add("https://focus.de/tests/test4", 300);
        add("https://focus.de/tests/test5", 1500);
        add("https://focus.de/tests/test6", 600);

        try {
            UsableURI u1 = new UsableURI("https://focus.de/tests/123", true);
            UsableURI u2 = new UsableURI("https://focus.de/tests", true);
            UURI uUri = new UURI(u1, u2);
            domainTestUri = new CrawlURI(uUri);

        } catch (URIException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testDomain() {
        String dom = identifier(domainTestUri);
        Assert.assertEquals("Expected domain: ", "focus.de", dom);
    }

    @Test
    public void testTextSize() {
        Assert.assertEquals("Expected total text length: ", 5644, textSize());
    }


    @Test
    public void testEmptyUrisTextSize() {
        uris = new ConcurrentHashMap<>();
        Assert.assertEquals("Expected total text length: ", 0, textSize());
    }

    @Test
    public void testNumberOfAccesses() {
        Assert.assertEquals("Expected accesses: ", 6, numberOfAccesses());
    }

    @Test
    public void testEmptyUrisNumberOfAccesses() {
        uris = new ConcurrentHashMap<>();
        Assert.assertEquals("Expected accesses: ", 0, numberOfAccesses());
    }


    @Test
    public void testNumberOfPagesWithTextLessThan() {
        Assert.assertEquals("Number of sites with less than 1000 text: ", 6,
                numberOfPagesWithTextLessThan(5000));

        Assert.assertEquals("Number of sites with less than 1000 text: ", 2,
                numberOfPagesWithTextLessThan(1000));

        Assert.assertEquals("Number of sites with less than 1000 text: ", 0,
                numberOfPagesWithTextLessThan(100));

    }

    @Test
    public void testEmptyUrisNumberOfPagesWithTextLessThan() {
        uris = new ConcurrentHashMap<>();
        Assert.assertEquals("Number of sites with less than 1000 text: ", 0,
                numberOfPagesWithTextLessThan(5000));

    }

    @Test
    public void testTextAccessRation() {
        Assert.assertEquals("Expected text access ratio: ", 940.66,
                textAccessesRatio(), 0.01);
    }


    @Test
    public void testLogTextAccessRatio() {
        Assert.assertEquals("Expected log access ratio : ", 9.380220514726086E-4,
                logTextAccessesRatio(), 0.01);
    }


    @Test
    public void ThresholdTextAccessRatio() {
        Assert.assertEquals("Expected log access ratio : ", 0.0,
                textThresholdAccessesRatio(5000), 0.01);

        Assert.assertEquals("Expected log access ratio : ", 0.6667,
                textThresholdAccessesRatio(1000), 0.01);

        Assert.assertEquals("Expected log access ratio : ", 1,
                textThresholdAccessesRatio(100), 0.01);

    }

}
