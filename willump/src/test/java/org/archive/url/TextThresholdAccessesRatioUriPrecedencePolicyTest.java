package org.archive.url;

import org.apache.commons.httpclient.URIException;
import org.archive.crawler.frontier.precedence.TextThresholdAccessesRatioUriPrecedencePolicy;
import org.archive.crawler.postprocessor.CrawlDirectoryInfo;
import org.archive.crawler.postprocessor.CrawlDomainInfo;
import org.archive.crawler.postprocessor.SolrProcessor;
import org.archive.modules.CrawlURI;
import org.archive.net.UURI;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TextThresholdAccessesRatioUriPrecedencePolicyTest extends TextThresholdAccessesRatioUriPrecedencePolicy {
    private CrawlURI domainTestUri;
    private CrawlURI domainTestUriTwo;
    private CrawlDirectoryInfo testDirectory;
    private CrawlDomainInfo testDomain;


    @Before
    public void setup() {
        try {
            testDirectory = new CrawlDirectoryInfo();
            testDirectory.add("https://focus.de/tests/test1", 1000);
            testDirectory.add("https://focus.de/tests/test2", 1234);
            testDirectory.add("https://focus.de/tests/test3", 1010);
            testDirectory.add("https://focus.de/tests/test4", 300);
            testDirectory.add("https://focus.de/tests/test5", 1500);
            testDirectory.add("https://focus.de/tests/test6", 600);

            testDomain = new CrawlDomainInfo();
            testDomain.add("https://focus.de/tests/test1", 1000);
            testDomain.add("https://focus.de/tests/test2", 1234);
            testDomain.add("https://focus.de/tests/test3", 1010);
            testDomain.add("https://focus.de/tests/test4", 300);
            testDomain.add("https://focus.de/tests/test5", 1500);
            testDomain.add("https://focus.de/tests/test6", 600);


            UsableURI u1 = new UsableURI("https://focus.de/tests/123", true);
            UsableURI u2 = new UsableURI("https://focus.de/tests", true);
            UURI uUri = new UURI(u1, u2);
            domainTestUri = new CrawlURI(uUri);
            u1 = new UsableURI("https://focus.de/tests/test5", true);
            u2 = new UsableURI("https://focus.de/tests/tests/test5", true);
            uUri = new UURI(u1, u2);
            domainTestUriTwo = new CrawlURI(uUri);
            domainTestUriTwo.setPrecedence(3);


        } catch (URIException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void uriScheduledTestDirectEmptyMap() {

        setCrawlItemGrouping(1);
        SolrProcessor testSolrProc;
        testSolrProc = new SolrProcessor();
        SolrProcessor.crawlItemMap.clear();

        itemIdentifier(domainTestUri);
        uriScheduled(domainTestUri);

        Assert.assertEquals("Expected Precedence ", 0, domainTestUri.getPrecedence());
    }

    @Test
    public void uriScheduledTestDomainEmptyMap() {

        setCrawlItemGrouping(2);
        SolrProcessor.crawlItemMap.clear();
        itemIdentifier(domainTestUri);
        uriScheduled(domainTestUri);

        Assert.assertEquals("Expected Precedence ", 0, domainTestUri.getPrecedence());
    }

    @Test
    public void uriScheduledTestDirect() {

        setCrawlItemGrouping(1);
        SolrProcessor.crawlItemMap.put("tests", testDirectory);

        itemIdentifier(domainTestUri);
        uriScheduled(domainTestUri);
        Assert.assertEquals("Expected Precedence ", 666, domainTestUri.getPrecedence());
        itemIdentifier(domainTestUriTwo);
        uriScheduled(domainTestUriTwo);
        Assert.assertEquals("Expected Precedence ", 669, domainTestUriTwo.getPrecedence());
    }

    @Test
    public void uriScheduledTestDomain() {


        setCrawlItemGrouping(2);
        SolrProcessor.crawlItemMap.put("focus.de", testDomain);
        itemIdentifier(domainTestUri);
        uriScheduled(domainTestUri);
        Assert.assertEquals("Expected Precedence ", 666, domainTestUri.getPrecedence());
        itemIdentifier(domainTestUriTwo);
        uriScheduled(domainTestUriTwo);
        Assert.assertEquals("Expected Precedence ", 669, domainTestUriTwo.getPrecedence());
    }


}



