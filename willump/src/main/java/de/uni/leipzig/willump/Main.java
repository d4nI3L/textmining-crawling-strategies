package de.uni.leipzig.willump;

import org.archive.crawler.Heritrix;

import java.util.logging.Level;
import java.util.logging.Logger;


public class Main extends Heritrix {

    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws Exception {
        logger.log(Level.ALL, "Starting from Willump!");

        System.out.println("Starting from Willump!");
        Heritrix.main(args);
    }

}
