package de.uni.leipzig.willump;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

public class CrawlCounter {

    private static CrawlCounter instance;

    public AtomicLong sumTextlength = new AtomicLong(0);
    public AtomicInteger counter = new AtomicInteger(0);
    static ReentrantLock counterLock = new ReentrantLock(true);


    private CrawlCounter() {
    }


    public synchronized static CrawlCounter getInstance() {
        if (CrawlCounter.instance == null) {
            CrawlCounter.instance = new CrawlCounter();
        }
        return CrawlCounter.instance;
    }

    public void doIncrementCounter() {
        counterLock.lock();
        try {
            counter.incrementAndGet();
        } finally {
            counterLock.unlock();
        }
    }

    public void doAddSumText(int text) {
        counterLock.lock();
        try {

            sumTextlength.addAndGet(text)
            ;
        } finally {
            counterLock.unlock();
        }
    }
}
