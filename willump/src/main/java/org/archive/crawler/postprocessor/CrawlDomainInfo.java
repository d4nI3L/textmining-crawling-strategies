package org.archive.crawler.postprocessor;

import org.archive.modules.CrawlURI;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CrawlDomainInfo extends CrawlItemInfo {
    static Logger logger = Logger.getLogger(CrawlDirectoryInfo.class.getName());

    public CrawlDomainInfo() {
    }

    public static String identifier(CrawlURI curi) {
        try {
            URI uri = createSafeURI(curi);
            return uri.getHost();
        } catch (URISyntaxException | MalformedURLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getCause());
        }
        return "";
    }
}