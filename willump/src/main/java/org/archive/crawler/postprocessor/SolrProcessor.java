package org.archive.crawler.postprocessor;

import org.archive.crawler.framework.Frontier;
import org.archive.crawler.reporting.CrawlerLoggerModule;
import org.archive.crawler.spring.SheetOverlaysManager;
import org.archive.modules.CandidateChain;
import org.archive.modules.CrawlURI;
import org.archive.modules.Processor;
import org.archive.modules.SchedulingConstants;
import org.archive.modules.extractor.Hop;
import org.archive.modules.seeds.SeedModule;
import org.archive.spring.KeyedProperties;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

/**
 * Processor for documents retrieved from a solr instance.
 */
public class SolrProcessor extends Processor {

    static Logger logger = Logger.getLogger(SolrProcessor.class.getName());
    public static ConcurrentHashMap<String, CrawlItemInfo> crawlItemMap = new ConcurrentHashMap<>();

    @SuppressWarnings("unused")
    private static final long serialVersionUID = -3L;

    /**
     * Candidate chain
     */
    protected CandidateChain candidateChain;

    public CandidateChain getCandidateChain() {
        return this.candidateChain;
    }

    @Autowired
    public void setCandidateChain(CandidateChain candidateChain) {
        this.candidateChain = candidateChain;
    }

    /**
     * The frontier to use.
     */
    protected Frontier frontier;

    public Frontier getFrontier() {
        return this.frontier;
    }

    @Autowired
    public void setFrontier(Frontier frontier) {
        this.frontier = frontier;
    }


    protected CrawlerLoggerModule loggerModule;

    public CrawlerLoggerModule getLoggerModule() {
        return this.loggerModule;
    }

    @Autowired
    public void setLoggerModule(CrawlerLoggerModule loggerModule) {
        this.loggerModule = loggerModule;
    }

    /**
     * If enabled, any URL found because a seed redirected to it (original seed
     * returned 301 or 302), will also be treated as a seed, as long as the hop
     * count is less than {@value #SEEDS_REDIRECT_NEW_SEEDS_MAX_HOPS}.
     */ {
        setSeedsRedirectNewSeeds(true);
    }

    public boolean getSeedsRedirectNewSeeds() {
        return (Boolean) kp.get("seedsRedirectNewSeeds");
    }

    public void setSeedsRedirectNewSeeds(boolean redirect) {
        kp.put("seedsRedirectNewSeeds", redirect);
    }

    protected static final int SEEDS_REDIRECT_NEW_SEEDS_MAX_HOPS = 5;

    /**
     * If true, outlinks from status codes <200 and >=400
     * will be sent through candidates processing. Default is
     * false.
     */ {
        setProcessErrorOutlinks(false);
    }

    public boolean getProcessErrorOutlinks() {
        return (Boolean) kp.get("processErrorOutlinks");
    }

    public void setProcessErrorOutlinks(boolean errorOutlinks) {
        kp.put("processErrorOutlinks", errorOutlinks);
    }

    protected SeedModule seeds;

    public SeedModule getSeeds() {
        return this.seeds;
    }

    @Autowired
    public void setSeeds(SeedModule seeds) {
        this.seeds = seeds;
    }

    protected SheetOverlaysManager sheetOverlaysManager;

    public SheetOverlaysManager getSheetOverlaysManager() {
        return sheetOverlaysManager;
    }

    @Autowired
    public void setSheetOverlaysManager(SheetOverlaysManager sheetOverlaysManager) {
        this.sheetOverlaysManager = sheetOverlaysManager;
    }

    /**
     * Usual no-argument constructor
     */
    public SolrProcessor() {
    }

    /* (non-Javadoc)
     * @see org.archive.modules.Processor#shouldProcess(org.archive.modules.CrawlURI)
     */
    protected boolean shouldProcess(CrawlURI puri) {
        return true;
    }

    /**
     * Run candidatesChain on a single candidate CrawlURI; if its
     * reported status is nonnegative, schedule to frontier.
     * <p>
     * Also applies special handling of discovered URIs that by
     * convention we want to treat as seeds (which then may be
     * scheduled indirectly via addSeed).
     *
     * @param candidate CrawlURI to consider
     * @param source    CrawlURI from which candidate was discovered/derived
     * @return candidate's status code at end of candidate chain execution
     * @throws InterruptedException
     */
    public int runCandidateChain(CrawlURI candidate, CrawlURI source) throws InterruptedException {
        // at least for duration of candidatechain, offer
        // access to full CrawlURI of via

        candidate.setFullVia(source);
        sheetOverlaysManager.applyOverlaysTo(candidate);
        try {
            KeyedProperties.clearOverridesFrom(source);
            KeyedProperties.loadOverridesFrom(candidate);

            // apply special seed-status promotion
            if (getSeedsRedirectNewSeeds() && source != null && source.isSeed()
                    && candidate.getLastHop().equals(Hop.REFER.getHopString())
                    && candidate.getHopCount() < SEEDS_REDIRECT_NEW_SEEDS_MAX_HOPS) {
                candidate.setSeed(true);
            }

            getCandidateChain().process(candidate, null);
            //int statusAfterCandidateChain = candidate.getFetchStatus();
            /*
            candidate.setForceFetch(true);
            getSeeds().addSeed(candidate);
            */
            frontier.schedule(candidate);
            return 200;
        } finally {
            KeyedProperties.clearOverridesFrom(candidate);
            KeyedProperties.loadOverridesFrom(source);
        }
    }

    /**
     * Run candidates chain on each of (1) any prerequisite, if present;
     * (2) any outCandidates, if present; (3) all outlinks, if appropriate
     *
     * @see org.archive.modules.Processor#innerProcess(org.archive.modules.CrawlURI)
     */
    @Override
    protected void innerProcess(final CrawlURI curi) throws InterruptedException {

        // check if item grouping is enabled and the text length field is set.
        if (this.isItemGroupingEnabled() && curi.getData().get("length") != null) {
            // Add the crawled uri and it's text length to the crawl item.
            String identifier = this.crawlItemIdentifier(curi);
            if (SolrProcessor.crawlItemMap.get(identifier) != null) {
                SolrProcessor.crawlItemMap.get(identifier).add(curi.getURI(), (int) curi.getData().get("length"));
            } else {
                try {
                    Class<? extends CrawlItemInfo> crawlItemClass = this.crawlItemClass();
                    if (crawlItemClass == null) {
                        throw new InstantiationError();
                    }
                    CrawlItemInfo dirInfo = crawlItemClass.getConstructor().newInstance();
                    dirInfo.add(curi.getURI(), (int) curi.getData().get("length"));
                    SolrProcessor.crawlItemMap.put(identifier, dirInfo);
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        }

        // Process all outlinks
        for (CrawlURI candidate : curi.getOutLinks()) {
            runCandidateChain(candidate, curi);
        }
    }

    public int getCrawlItemGrouping() {
        return (int) kp.get("crawlItemGrouping");
    }

    public void setCrawlItemGrouping(int grouping) {
        kp.put("crawlItemGrouping", grouping);
    }

    /**
     * Returns the CrawlInfoItem subclass for the crawlItemGrouping specified in the crawler beans or null.
     * null will be returned when crawlItemGrouping is set to 0 (no grouping) or an invalid value.
     *
     * @return The CrawlInfoItem subclass or null.
     */
    private Class<? extends CrawlItemInfo> crawlItemClass() {
        switch (this.getCrawlItemGrouping()) {
            case 1:
                return CrawlDirectoryInfo.class;
            case 2:
                return CrawlDomainInfo.class;
        }
        return null;
    }

    /**
     * Returns the identifier for grouping the crawl url provided.
     *
     * @param curi The crawl uri to generate the identifier for.
     * @return The identifier for grouping the curi.
     */
    private String crawlItemIdentifier(CrawlURI curi) {
        try {
            Class<? extends CrawlItemInfo> crawlItemClass = this.crawlItemClass();
            if (crawlItemClass == null) {
                return "";
            }

            Method method = crawlItemClass.getMethod("identifier", CrawlURI.class);
            return (String) method.invoke(null, curi);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Determines whether item grouping has been enabled in the crawler beans.
     *
     * @return Boolean value indicating whether item grouping has been enabled.
     */
    private boolean isItemGroupingEnabled() {
        return this.crawlItemClass() != null;
    }


    /**
     * Check if the URI needs special 'discovered seed' treatment.
     *
     * @param curi The curi to be checked.
     */
    protected boolean checkForSeedPromotion(CrawlURI curi) {
        if (curi.isSeed() && curi.getVia() != null
                && curi.flattenVia().length() > 0) {
            // The only way a seed can have a non-empty via is if it is the
            // result of a seed redirect. Returning true here schedules it
            // via the seeds module, so it may affect scope and be logged
            // as 'discovered' seed.
            //
            // This is a feature. This is handling for case where a seed
            // gets immediately redirected to another page. What we're doing is
            // treating the immediate redirect target as a seed.

            // And it needs rapid scheduling.
            if (curi.getSchedulingDirective() == SchedulingConstants.NORMAL) {
                curi.setSchedulingDirective(SchedulingConstants.MEDIUM);
            }
            return true;
        }
        return false;
    }
}
