package org.archive.crawler.postprocessor;

import org.archive.modules.CrawlURI;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CrawlDirectoryInfo extends CrawlItemInfo {
    static Logger logger = Logger.getLogger(CrawlDirectoryInfo.class.getName());

    public CrawlDirectoryInfo() {
    }


    public static String identifier(CrawlURI curi) {
        try {
            URI uri = createSafeURI(curi);
            String[] pathComponents = uri.getPath().split("/");

            if (pathComponents.length <= 1) {
                return uri.getHost();
            }
            return pathComponents[1];
        } catch (URISyntaxException | MalformedURLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e.getCause());
        }
        return "";
    }

}