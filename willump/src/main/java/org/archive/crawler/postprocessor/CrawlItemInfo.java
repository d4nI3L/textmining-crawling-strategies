package org.archive.crawler.postprocessor;

import org.archive.modules.CrawlURI;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;

abstract public class CrawlItemInfo {
    /**
     * Thread safe HashMap to store the uris.
     */
    public ConcurrentHashMap<String, Integer> uris = new ConcurrentHashMap<>();

    /**
     * Generates an identifier for a given CrawlURI to group the Info items by.
     *
     * @param curi The CrawlURI to generate the identifier for.
     * @return The identifier.
     */
    static public String identifier(CrawlURI curi) {
        return "";
    }


    /**
     * Add a new CrawlURI to the `uris` map.
     *
     * @param uri        The CrawlURI to be added.
     * @param textLength The length of the text that was extracted from the uri.
     */
    public void add(String uri, Integer textLength) {
        this.uris.put(uri, textLength);
    }

    /**
     * Calculate the accumulated amount of text across all the uris that have been added to this Info.
     *
     * @return The accumulated amount of text.
     */
    public int textSize() {
        return this.uris.values().stream().reduce(0, Integer::sum);
    }

    /**
     * Returns the total number of uris that have been added to this Info.
     *
     * @return The total number of uris.
     */
    public int numberOfAccesses() {
        return this.uris.values().size();
    }

    /**
     * Calculates the number of uris that contain less text than a certain threshold value.
     *
     * @param textLength The text length threshold for the calculation.
     * @return The number of uris.
     */
    public int numberOfPagesWithTextLessThan(int textLength) {
        return this.uris.values().stream().filter(i -> i < textLength).toArray().length;
    }


    /**
     * Calculates the ratio between the total amount of text and the number of crawled pages.
     * The value will be higher the more text is found on the individual pages.
     *
     * @return The ratio between the total amount of text and the number of hits.
     */
    public double textAccessesRatio() {
        return (double) this.textSize() / (double) this.numberOfAccesses();
    }

    /**
     * Calculates the ratio between the logarithmic total amount of text and the number of crawled pages.
     * We add 1 to the total amount of text to guarantee that the denominator is positive.
     * This measure will first increase rapidly and will grow less as the total amount of text increases.
     *
     * @return The ratio between the logarithmic total amount of text and the number of crawled pages.
     */
    public double logTextAccessesRatio() {
        return Math.log((double) this.textSize() / 1000000 + 1.0) / (double) this.numberOfAccesses();
        //return Math.log((double) this.textSize() / ((double) this.numberOfAccesses() * 1000.0) + 1.0) * 5000.0;
    }

    /**
     * Calculates the ratio between the number of pages with at least a certain amount of text and the number of crawled pages.
     *
     * @param textLength The least amount of text the pages should contain.
     * @return The ratio between the number of pages with at least a certain amount of text and the number of crawled pages.
     */
    public double textThresholdAccessesRatio(int textLength) {
        return 1.0 - ((double) numberOfPagesWithTextLessThan(textLength) / (double) numberOfAccesses());
    }

    /**
     * Savely casts a CrawlURI to a URI and return it.
     *
     * @param curi The given CrawlURI
     * @return URI the URI of the CrawlURI
     * @throws MalformedURLException if the string representation of the URI cant be casted to URL
     * @throws URISyntaxException    if the URL cant be casted to a URI
     */
    public static URI createSafeURI(CrawlURI curi) throws MalformedURLException, URISyntaxException {
        URL url = new URL(escapePipe(curi.getURI()));
        return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(),
                url.getPath(), url.getQuery(), url.getRef());
    }

    /**
     * Escapes the pipe chracter in a String
     *
     * @param uri A String represention of a URI
     * @return escaped string for casting to a URL
     */
    public static String escapePipe(String uri) {
        return uri.replace("|", "%7C");
    }
}