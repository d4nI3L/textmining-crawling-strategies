package org.archive.crawler.framework;

import de.uni.leipzig.willump.CrawlCounter;

import java.util.logging.Level;
import java.util.logging.Logger;

public class WillumpCrawlController extends CrawlController {

    private static WillumpCrawlController instance;

    private static CrawlCounter crawlCounter = CrawlCounter.getInstance();
    static Logger logger = Logger.getLogger(WillumpCrawlController.class.getName());


    public synchronized static WillumpCrawlController getInstance() {
        if (WillumpCrawlController.instance == null) {
            WillumpCrawlController.instance = new WillumpCrawlController();
        }
        return WillumpCrawlController.instance;
    }

    private WillumpCrawlController() {
    }

    public synchronized void checkToStop() {

        int sites = crawlCounter.counter.get();
        long textSum = crawlCounter.sumTextlength.get();
        switch (sites) {

            case 1000:
            case 10000:
            case 100000:
                //case 1000000:
                logger.log(Level.INFO, "#### Reached " + sites
                        + "##### Extracted text: " + textSum);
                break;

        }

    }
}
