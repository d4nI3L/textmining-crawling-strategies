package org.archive.crawler.framework;

import de.uni.leipzig.willump.CrawlCounter;
import org.archive.crawler.reporting.CrawlStatSnapshot;

public class WillumpCrawlLimitEnforcer extends CrawlLimitEnforcer {

    private CrawlCounter counter = CrawlCounter.getInstance();


    protected void checkForLimitsExceeded(CrawlStatSnapshot snapshot) {
        if (maxBytesDownload > 0 && snapshot.bytesProcessed >= maxBytesDownload) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_DATA_LIMIT);
        } else if (maxNovelBytes > 0 && snapshot.novelBytes >= maxNovelBytes) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_DATA_LIMIT);
        } else if (maxWarcNovelBytes > 0 && snapshot.warcNovelBytes >= maxWarcNovelBytes) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_DATA_LIMIT);
        } else if (maxDocumentsDownload > 0
                && (long) counter.counter.get() >= maxDocumentsDownload) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_DOCUMENT_LIMIT);
        } else if (maxNovelUrls > 0
                && snapshot.novelUriCount >= maxNovelUrls) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_DOCUMENT_LIMIT);
        } else if (maxWarcNovelUrls > 0
                && snapshot.warcNovelUriCount >= maxWarcNovelUrls) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_DOCUMENT_LIMIT);
        } else if (maxTimeSeconds > 0
                && snapshot.elapsedMilliseconds >= maxTimeSeconds * 1000) {
            controller.requestCrawlStop(CrawlStatus.FINISHED_TIME_LIMIT);
        }
    }

}
