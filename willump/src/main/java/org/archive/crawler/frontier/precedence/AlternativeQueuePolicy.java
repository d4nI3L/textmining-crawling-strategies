package org.archive.crawler.frontier.precedence;

import org.archive.crawler.frontier.WorkQueue;
import org.archive.modules.CrawlURI;
import org.archive.spring.KeyedProperties;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.logging.Logger;

public class AlternativeQueuePolicy extends QueuePrecedencePolicy {
    transient protected CrawlURI peekItem = null;
    public static String log = "";
    private static final Logger logger =
            Logger.getLogger(WorkQueue.class.getName());

    private static final long serialVersionUID = 8312032856661175869L;

    protected KeyedProperties kp = new KeyedProperties();

    public KeyedProperties getKeyedProperties() {
        return kp;
    }

    /** constant precedence to assign; default is 3 (which leaves room
     * for a simple overlay to prioritize queues) */ {
        setBasePrecedence(3);
    }

    public int getBasePrecedence() {
        return (Integer) kp.get("basePrecedence");
    }

    public void setBasePrecedence(int precedence) {
        kp.put("basePrecedence", precedence);
    }

    /* (non-Javadoc)
     * @see org.archive.crawler.frontier.QueuePrecedencePolicy#queueCreated(org.archive.crawler.frontier.WorkQueue)
     */
    @Override
    public void queueCreated(WorkQueue wq) {
        installProvider(wq);
    }

    /**
     * Install the appropriate provider helper object into the WorkQueue,
     * if necessary.
     *
     * @param wq target WorkQueue this policy will operate on
     */
    protected void installProvider(WorkQueue wq) {
        SimplePrecedenceProvider precedenceProvider =
                new SimplePrecedenceProvider(calculatePrecedence(wq));
        wq.setPrecedenceProvider(precedenceProvider);
    }

    /**
     * Calculate the precedence value for the given queue.
     *
     * @param wq WorkQueue
     * @return int precedence value
     */
    protected int calculatePrecedence(WorkQueue wq) {
        String path = "/home/max/Documents/outputFile.txt";
        try {
            log = log + wq.getClassKey();
            FileWriter write = new FileWriter(path);
            PrintWriter printer = new PrintWriter(write);
            printer.write(log + " irgendeineNummer");
            printer.close();
        } catch (Exception e) {
        }
        if (wq.getClassKey().equals("*.html")) {
            kp.put("basePrecedence", 1);
        } else {
            kp.put("basePrecedence", 5);
        }
        return getBasePrecedence();
    }

    /* (non-Javadoc)
     * @see org.archive.crawler.frontier.QueuePrecedencePolicy#queueReevaluate(org.archive.crawler.frontier.WorkQueue)
     */
    @Override
    public void queueReevaluate(WorkQueue wq) {
        PrecedenceProvider precedenceProvider =
                wq.getPrecedenceProvider();
        // TODO: consider if this fails to replace provider that is
        // a subclass of Simple when necessary
        if (precedenceProvider instanceof SimplePrecedenceProvider) {
            // reset inside provider
            ((SimplePrecedenceProvider) precedenceProvider).setPrecedence(
                    calculatePrecedence(wq));
        } else {
            // replace provider
            installProvider(wq);
        }
    }

}
