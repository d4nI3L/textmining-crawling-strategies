package org.archive.crawler.frontier.precedence;

import org.archive.crawler.postprocessor.CrawlDirectoryInfo;
import org.archive.crawler.postprocessor.CrawlDomainInfo;
import org.archive.modules.CrawlURI;
import org.archive.spring.HasKeyedProperties;
import org.archive.spring.KeyedProperties;

public abstract class KeyedUriPrecedencePolicy extends UriPrecedencePolicy implements HasKeyedProperties {
    protected KeyedProperties kp = new KeyedProperties();

    @Override
    public KeyedProperties getKeyedProperties() {
        return kp;
    }

    public int getCrawlItemGrouping() {
        return (int) kp.get("crawlItemGrouping");
    }

    public void setCrawlItemGrouping(int grouping) {
        kp.put("crawlItemGrouping", grouping);
    }

    /**
     * Returns an identifier for a given curi based on the crawlItemGrouping property, loaded from the crawler beans config file.
     *
     * @param curi The curi to determine the identifier for.
     * @return An identifier based on the crawlItemGrouping property, empty string if unknown.
     */
    protected String itemIdentifier(CrawlURI curi) {

        switch (this.getCrawlItemGrouping()) {
            case 1:
                return CrawlDirectoryInfo.identifier(curi);
            case 2:
                return CrawlDomainInfo.identifier(curi);
        }
        return "";
    }
}