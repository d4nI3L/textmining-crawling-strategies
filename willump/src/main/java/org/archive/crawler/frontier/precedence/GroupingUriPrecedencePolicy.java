package org.archive.crawler.frontier.precedence;

import org.archive.crawler.postprocessor.CrawlDirectoryInfo;
import org.archive.crawler.postprocessor.CrawlDomainInfo;
import org.archive.crawler.postprocessor.CrawlItemInfo;
import org.archive.modules.CrawlURI;

public abstract class GroupingUriPrecedencePolicy extends UriPrecedencePolicy {

    /**
     * Returns an identifier for a given curi based on the type of the crawlItemInfo, empty string if unknown.
     *
     * @param crawlItemInfo The CrawlItemInfo subclass which is used to determine the correct identifier.
     * @param curi          The curi to get the identifier for.
     * @return An identifier based on the type of the crawlItemInfo, empty string if unknown.
     */
    protected String identifier(CrawlItemInfo crawlItemInfo, CrawlURI curi) {

        if (crawlItemInfo instanceof CrawlDirectoryInfo) {
            return CrawlDirectoryInfo.identifier(curi);
        } else if (crawlItemInfo instanceof CrawlDomainInfo) {
            return CrawlDomainInfo.identifier(curi);
        }
        return "";
    }
}
