package org.archive.crawler.frontier.precedence;

import org.archive.crawler.postprocessor.CrawlItemInfo;
import org.archive.crawler.postprocessor.SolrProcessor;
import org.archive.modules.CrawlURI;

public class TextThresholdAccessesRatioUriPrecedencePolicy extends KeyedUriPrecedencePolicy {


    @Override
    public void uriScheduled(CrawlURI curi) {
        int precedence = curi.getPrecedence();
        String identifier = this.itemIdentifier(curi);
        CrawlItemInfo item = SolrProcessor.crawlItemMap.get(identifier);
        if (item != null && item.numberOfPagesWithTextLessThan(1000) != 0) {
            precedence += item.textThresholdAccessesRatio(1000) * 1000;
            if (precedence > 5000) {
                precedence = 5000;
            }

        }
        curi.setPrecedence(precedence);
    }
}
