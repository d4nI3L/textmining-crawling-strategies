package org.archive.crawler.frontier;

import org.archive.crawler.frontier.precedence.HighestUriQueuePrecedencePolicy;
import org.archive.modules.CrawlURI;
import org.archive.net.UURI;
import org.archive.util.TextUtils;

import java.util.regex.Matcher;

public class AntiCalendarAndQueryCostAssignmentPolicy extends CostAssignmentPolicy {

    private HighestUriQueuePrecedencePolicy pr;

    private static final long serialVersionUID = 2315894081209701073L;

    public static String CALENDARISH =
            "(?i)(kalender)| (calendar)|(year)|(jahr)|(month)|(monat)|(day)|(date)|(viewcal)" +
                    "|(\\D19\\d\\d\\D)|(\\D20\\d\\d\\D)|(event)|(yr=)" +
                    "|(calendrier)|(jour)";

    private static final String IMAGE_PATTERN =
            "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";


    public int costOf(CrawlURI curi) {
        int cost = 1;
        UURI uuri = curi.getUURI();
        if (uuri.hasQuery()) {
            // has query string
            cost++;
            int qIndex = uuri.toString().indexOf('?');
            if (curi.flattenVia().startsWith(uuri.toString().substring(0, qIndex))) {
                // non-query-string portion of URI is same as previous
                cost++;
            }

        }
        // penealty for logins
        if (curi.toString().contains("login")) {
            cost += 10;
            //and if parent curi is also login
            if (curi.flattenVia().contains("login")) {
                cost += 10;
            }
        }

        // increment cost for every calenderish substring
        Matcher m = TextUtils.getMatcher(CALENDARISH, curi.toString());
        if (m.find()) {
            while (m.find()) {
                cost++;
            }
        }
        TextUtils.recycleMatcher(m);

        // high penalty for images
        Matcher matcher = TextUtils.getMatcher(IMAGE_PATTERN, curi.toString());
        if (curi.toString().contains("img") || m.find()) {
            cost += 100;
        }
        TextUtils.recycleMatcher(matcher);
        return cost;
    }

}
