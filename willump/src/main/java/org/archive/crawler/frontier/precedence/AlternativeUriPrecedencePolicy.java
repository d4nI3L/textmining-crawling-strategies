/*
 *  This file is part of the Heritrix web crawler (crawler.archive.org).
 *
 *  Licensed to the Internet Archive (IA) by one or more individual
 *  contributors.
 *
 *  The IA licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.archive.crawler.frontier.precedence;

import org.archive.modules.CrawlURI;
import org.archive.spring.HasKeyedProperties;
import org.archive.spring.KeyedProperties;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.logging.Logger;

/**
 * UriPrecedencePolicy which assigns URIs a set value (perhaps a overridden
 * for different URIs).
 */
public class AlternativeUriPrecedencePolicy extends UriPrecedencePolicy
        implements HasKeyedProperties {
    public static String log = "";
    private static final long serialVersionUID = -8247330811715982746L;

    protected KeyedProperties kp = new KeyedProperties();

    public KeyedProperties getKeyedProperties() {
        return kp;
    }

    static Logger logger = Logger.getLogger(AlternativeUriPrecedencePolicy.class.getName());

    /** constant precedence to assign; default is 1 */ {
        setBasePrecedence(5);
    }

    public int getBasePrecedence() {
        return (Integer) kp.get("basePrecedence");
    }

    public void setBasePrecedence(int precedence) {
        kp.put("basePrecedence", precedence);
    }

    /* (non-Javadoc)
     * @see org.archive.crawler.frontier.precedence.UriPrecedencePolicy#uriScheduled(org.archive.crawler.datamodel.CrawlURI)
     */
    @Override
    public void uriScheduled(CrawlURI curi) {
        curi.setPrecedence(calculatePrecedence(curi));
    }

    /**
     * Calculate the precedence value for the given URI.
     *
     * @param curi CrawlURI to evaluate
     * @return int precedence for URI
     */
    protected int calculatePrecedence(CrawlURI curi) {


        String path = "/home/max/Documents/outputFile1.txt";
        try {
            FileWriter write = new FileWriter(path);
            PrintWriter printer = new PrintWriter(write);
            printer.write(log + " irgendeineNummer");
            printer.close();
        } catch (Exception e) {
        }
        if (curi.toString().contains(".html")) {
            kp.put("basePrecedence", 1);
        } else if (curi.toString().contains(".pdf")) {
            kp.put("basePrecedence", 2);
        } else if (curi.toString().contains("doc")) {
            kp.put("basePrecedence", 3);
        } else {
            kp.put("basePrecedence", 4);
        }

        log = log + curi.toString() + " " + getBasePrecedence() + "\n";
        return getBasePrecedence();
    }
}