package org.archive.crawler.frontier.precedence;

import org.archive.crawler.postprocessor.CrawlItemInfo;
import org.archive.crawler.postprocessor.SolrProcessor;
import org.archive.modules.CrawlURI;


public class TextAccessRatioUriPrecedencePolicy extends KeyedUriPrecedencePolicy {
    @Override
    public void uriScheduled(CrawlURI curi) {
        int precedence = curi.getPrecedence();

        String identifier = this.itemIdentifier(curi);
        CrawlItemInfo item = SolrProcessor.crawlItemMap.get(identifier);
        if (item != null) {
            if ((int) item.textAccessesRatio() < 5000) {
                precedence = 5005 - (int) item.textAccessesRatio();

            }
        }
        curi.setPrecedence(precedence);
    }
}
