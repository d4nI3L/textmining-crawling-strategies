package org.archive.crawler.frontier.precedence;

import org.archive.crawler.postprocessor.CrawlItemInfo;
import org.archive.crawler.postprocessor.SolrProcessor;
import org.archive.modules.CrawlURI;


public class LogTextToAccessesRatioUriPrecedencePolicy extends KeyedUriPrecedencePolicy {


    @Override
    public void uriScheduled(CrawlURI curi) {
        int precedence = curi.getPrecedence();

        String identifier = this.itemIdentifier(curi);
        CrawlItemInfo item = SolrProcessor.crawlItemMap.get(identifier);
        if (item != null) {
            precedence += 5005 - (int) (item.logTextAccessesRatio() * 100000);
            if (precedence < 0) {
                precedence = 0;
            }
        }
        curi.setPrecedence(precedence);
    }
}
