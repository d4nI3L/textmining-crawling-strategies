/*
 *  This file is part of the Heritrix web crawler (crawler.archive.org).
 *
 *  Licensed to the Internet Archive (IA) by one or more individual
 *  contributors.
 *
 *  The IA licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.archive.modules.extractor;

import de.uni.leipzig.willump.CrawlCounter;
import org.apache.commons.httpclient.URIException;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.archive.modules.CrawlURI;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Extracts URIs from HTTP response headers.
 *
 * @author gojomo
 */
public class ExtractorSolr extends Extractor {

    CrawlCounter cc = CrawlCounter.getInstance();

    static Logger logger = Logger.getLogger(ExtractorSolr.class.getName());

    @SuppressWarnings("unused")
    private static final long serialVersionUID = 3L;

    public ExtractorSolr() {
    }

    /**
     * should all HTTP URIs be used to infer a link to the site's root?
     */
    protected boolean inferRootPage = false;

    public boolean getInferRootPage() {
        return inferRootPage;
    }

    public void setInferRootPage(boolean inferRootPage) {
        this.inferRootPage = inferRootPage;
    }


    @Override
    protected boolean shouldProcess(CrawlURI uri) {
        return true;
    }


    @Override
    protected void extract(CrawlURI curi) {
        SolrDocumentList solrDocuments = (SolrDocumentList) curi.getData().get("solrDoc");

        for (SolrDocument doc : solrDocuments) {
            ArrayList<String> list = (ArrayList<String>) doc.getFieldValue("links");
            int textLength = (int) doc.getFieldValue("text_length");
            curi.getData().put("length", textLength);
          /*  cc.sumTextlength += textLength;
            logger.log(Level.INFO, "TEXTLENGTH: " + cc.sumTextlength); */
            for (String link : list) {
                //logger.log(Level.INFO, link);
                try {
                    CrawlURI next = curi.createCrawlURI(link, LinkContext.EMBED_MISC, Hop.REFER);
                    curi.getOutLinks().add(next);
                    //logger.log(Level.INFO, "Link added!");
                } catch (URIException e) {
                    //logUriError(e, curi.getUURI(), link);
                    logger.log(Level.SEVERE, e.toString(), curi.getURI());
                }
            }
        }
        curi.setFetchStatus(200);
        //logger.log(Level.INFO, "outlinks: " + curi.getOutLinks().toString());
    }

}
