package org.archive.modules.fetcher;


import de.uni.leipzig.willump.CrawlCounter;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.archive.crawler.framework.WillumpCrawlController;
import org.archive.modules.CrawlURI;
import org.archive.modules.Processor;

import java.io.IOException;
import java.util.logging.Logger;

public class FetchSolr extends Processor {

    private static CrawlCounter cc = CrawlCounter.getInstance();
    private static WillumpCrawlController controller = WillumpCrawlController.getInstance();

    static Logger logger = Logger.getLogger(FetchSolr.class.getName());

    public FetchSolr() {
        //BasicConfigurator.configure();
    }

    @Override
    protected boolean shouldProcess(CrawlURI uri) {

        return true;
    }

    public static SolrDocumentList doSolrQuery(String uri) throws IOException, SolrServerException {
        cc.doIncrementCounter();
        controller.checkToStop();
        String q = "id:" + escapeQueryUri(uri); //modifiziere URI
        final SolrQuery query = new SolrQuery(q); //initialisiere solr query mit uri
        final QueryResponse response = SolrConnection.getSolrClient().query("discovery", query); //Anfodern der Daten(Textlänge, Outgoing links) URI
        final SolrDocumentList documents = response.getResults();
        return documents;
    }

    public static String escapeQueryUri(String uri) {
        String res = uri.replace(":", "\\:");
        res = res.replace("(", "\\(");
        res = res.replace(")", "\\)");
        res = res.replace("|", "\\|");
        res = res.replace("[", "\\[");
        res = res.replace("]", "\\]");


        return res;
    }


    protected void innerProcess(final CrawlURI curi) throws InterruptedException {
        try {
            SolrDocumentList solrDocuments = doSolrQuery(curi.getURI());
            curi.getData().put("solrDoc", solrDocuments);
            for (SolrDocument sd : solrDocuments) {
                int text = (int) sd.getFieldValue("text_length");
                cc.doAddSumText(text);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
    }
}
