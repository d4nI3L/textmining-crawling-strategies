package org.archive.modules.fetcher;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.SolrPing;
import org.apache.solr.client.solrj.response.SolrPingResponse;

import java.io.IOException;


public final class SolrConnection {

    private static final String SOLR_BASE_URI = "http://aspra16.informatik.uni-leipzig.de:8983/solr";
    private static final int CONNECTION_TIMEOUT = 10000;
    private static final int SOCKET_TIMEOUT = 60000;
    private static HttpSolrClient solrClient = null;

    /**
     * Creates a SolrClient Connection for the SolrConnection param and verifies if the service is running
     *
     * @return A SolrClient for the SolrConnection class param
     */
    public static SolrClient getSolrClient() {

        if (solrClient == null) {
            solrClient = new HttpSolrClient.Builder(SOLR_BASE_URI)
                    .withConnectionTimeout(CONNECTION_TIMEOUT)
                    .withSocketTimeout(SOCKET_TIMEOUT)
                    .build();


            // check connection and ping the server
            //ping(solrClient);

            return solrClient;
        }
        return solrClient;
    }

    /**
     * Issues a ping request to check if the server is alive
     *
     * @throws IOException If there is a low-level I/O error.
     */
    public static SolrPingResponse ping(SolrClient client) throws SolrServerException, IOException {
        return new SolrPing().process(client);
    }

    public String getSOLR_BASE_URI() {
        return SOLR_BASE_URI;
    }


    public int getCONNECTION_TIMEOUT() {
        return CONNECTION_TIMEOUT;
    }


    public int getSOCKET_TIMEOUT() {
        return SOCKET_TIMEOUT;
    }


}

