<html>
    <head>
        <style>
            h2 {
                margin-bottom: 0;
                display: inline-block;
            }

            input[type="text"] {
                width: 300px;
            }

            input[type="text"]:disabled {
                width: 200px;
            }
        </style>
    </head>
    <body>
        <form action="save.php" method="POST">

            <?php
                $basicBeans = ["candidates", "preparer", "costAssignmentPolicy", "frontier"];
                $isBasicMode = $_POST["mode"] == "basic";
                $referenceFileName = $_POST["file"];
                echo "<input type=\"hidden\" name=\"file\" value=\"$referenceFileName\">";

                $dom = DOMDocument::load("beans" . DIRECTORY_SEPARATOR . $referenceFileName);

                $beans = $dom->getElementsByTagName("bean");
                foreach ($beans as $bean) {
                    if ($bean->getAttribute("id") == null || $bean->getAttribute("class") == null) {
                        continue;
                    }

                    $beanId = $bean->getAttribute("id");
                    $beanClass = $bean->getAttribute("class");

                    if ($isBasicMode && !in_array($beanId, $basicBeans)) { continue; }

                    echo '<h2>Bean ' . $beanId . '</h2> => class: <input type="text" value="' . $beanClass . '" name="' . $beanId . '#_#class"/><br />';
                    
                    $properties = $bean->getElementsByTagName("property");
                    if (count($properties) > 0) {
                        foreach ($properties as $property) {

                            $propertyName = $property->getAttribute("name");

                            if ($property->getAttribute("value") != null) {

                                // This is a value-property
                                $propertyValue = $property->getAttribute("value");
                                echo '<input type="text" value="' . $propertyName . '" disabled /> = <input type="text" value="' . $propertyValue . '" name="' . $beanId . "#_#valueproperty#_#" . $propertyName . '"><br />';
                            } else if ($property->getElementsByTagName("ref")->length != 0) {

                                // This is a referencing property
                                $ref = $property->getElementsByTagName("ref")[0];
                                $beanRef = $ref->getAttribute("bean");
                                echo '<input type="text" value="' . $propertyName . '" disabled /> references bean with id: <input type="text" value="' . $beanRef . '" name="' . $beanId . "#_#beanrefproperty#_#" . $propertyName . '"><br />';
                            } else if ($property->getElementsByTagName("bean")->length != 0) {

                                // This is a bean property
                                $ref = $property->getElementsByTagName("bean")[0];
                                $beanClass = $ref->getAttribute("class");
                                echo '<input type="text" value="' . $propertyName . '" disabled /> is bean with class: <input type="text" value="' . $beanClass . '" name="' . $beanId . "#_#beanclassproperty#_#" . $propertyName . '"><br />';
                            } 

                        }
                    } else {
                        echo "Bean has no properties.<br />";
                    }
                }

            ?>
            
            <input type="submit" value="Submit" />

        </form>
    </body>
</html>