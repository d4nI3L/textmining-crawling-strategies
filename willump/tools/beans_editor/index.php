<html>
    <body>
        <h1>Bean edit tool</h2>
        <form action="edit.php" method="POST">
            <p>Reference file:</p>
            <fieldset>
                <?php
                    $directory = './beans/';
                    $files = scandir($directory);

                    foreach ($files as $file) {
                        if (end(explode(".", $file)) != "cxml") { continue; }
                        echo "<input type=\"radio\" name=\"file\" id=\"$file\" value=\"$file\"> <label for=\"$file\"> $file</label><br />";
                    }
                ?>
            </fieldset>

            <p>Mode:</p>
            <fieldset>
                <input type="radio" name="mode" id="basic" value="basic"> <label for="basic"> Basic</label><br />
                <input type="radio" name="mode" id="advanced" value="advanced"> <label for="advanced"> Advanced</label>
            </fieldset>
            <br />
            <br />
            <input type="submit" value="Edit template" />
        </form>
    </body>
</html>
