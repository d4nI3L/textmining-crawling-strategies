<?php

    define("KEY_SEPARATOR", "#_#");
    $referenceFileName = $_POST["file"];

    $dom = DOMDocument::load("beans" . DIRECTORY_SEPARATOR . $referenceFileName);
    $beans = $dom->getElementsByTagName("bean");

    foreach ($_POST as $key => $value) {

        $keyComponents = explode(KEY_SEPARATOR, $key);
        $beanId = $keyComponents[0];

        foreach ($beans as $cbean) {
            if ($cbean->getAttribute("id") == $beanId) {
                $bean = $cbean;
                break; 
            }
        }

        if (!isset($bean)) { continue; }

        switch ($keyComponents[1]) {
        case "class":
            $bean->setAttribute("class", $value);
            break;

        case "valueproperty":
            $propertyName = $keyComponents[2];
            
            $properties = $bean->getElementsByTagName("property");
            foreach ($properties as $cproperty) {
                if ($cproperty->getAttribute("name") == $propertyName) {
                    $property = $cproperty;
                    break;
                }
            }
            if (!isset($property)) { break; }

            $property->setAttribute("value", $value);
            break;

        case "beanrefproperty":
            $propertyName = $keyComponents[2];
            
            $properties = $bean->getElementsByTagName("property");
            foreach ($properties as $cproperty) {
                if ($cproperty->getAttribute("name") == $propertyName) {
                    $property = $cproperty;
                    break;
                }
            }
            if (!isset($property)) { break; }

            $ref = $property->getElementsByTagName("ref")[0];
            $ref->setAttribute("bean", $value);
            break;

        case "beanclassproperty":
            $propertyName = $keyComponents[2];
            
            $properties = $bean->getElementsByTagName("property");
            foreach ($properties as $cproperty) {
                if ($cproperty->getAttribute("name") == $propertyName) {
                    $property = $cproperty;
                    break;
                }
            }
            if (!isset($property)) { break; }

            $ref = $property->getElementsByTagName("bean")[0];
            $ref->setAttribute("class", $value);
            break;

        default:
            break;
        } 
    }

    header("Content-Disposition: attachment;filename=crawler-beans_custom.cxml");
    header("Content-Type: text/xml");
    echo $dom->saveXML();
?>