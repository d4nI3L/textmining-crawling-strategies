#!/usr/bin/env python
__author__ = "Fabian Thies"
__copyright__ = "Copyright 2020, Text-Mining Gruppe 1: Crawling-Strategien"
__version__ = "1.0.0"


import pathlib
import xml.etree.cElementTree as ET
from typing import List


class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


script_path = pathlib.Path(__file__).parent.absolute()

ET.register_namespace("", "http://www.springframework.org/schema/beans")
ET.register_namespace("xsi", "http://www.w3.org/2001/XMLSchema-instance")
ET.register_namespace("context", "http://www.springframework.org/schema/context")
ET.register_namespace("aop", "http://www.springframework.org/schema/aop")
ET.register_namespace("tx", "http://www.springframework.org/schema/tx")

xml = ET.parse(script_path/'crawler-beans_template.cxml')
root = xml.getroot()
print("Template crawler-beans_template.cxml has been loaded.\n\n")



def saveXML(xml: ET):
    # Save the xml to a file named crawler-beans.cxml.
    full_path = script_path/"crawler-beans.cxml"
    xml.write(open(full_path, 'w'), encoding='unicode', xml_declaration=True)

    # Remove empty lines
    with open(full_path) as xmlfile:
        lines = [line for line in xmlfile if line.strip() is not ""]

    with open(full_path, "w") as xmlfile:
        xmlfile.writelines(lines)

    print(f"{color.BOLD}{color.GREEN}crawler-beans.cxml has been generated successfully.{color.END}\nThe file is located at: {full_path}")


def userListItemSelection(items: List[str], name: str):
    print(color.BOLD + f"Select a {name.lower()} to use:" + color.END)
    for index, item in enumerate(items, start=1):
        print(f"{color.BOLD}{index}:{color.END} {item}")
    selected_item = int(input(color.BOLD + "{} (1 - {}): ".format(name, len(items)) + color.END))
    selected_item -= 1
    print(f"{color.BOLD}{color.BLUE}{name} \"{items[selected_item]}\" has been selected.{color.END}\n\n")
    return selected_item




# Select a configuration
configurations = ["Baseline", "Directory", "Domain"]
selected_configuration = userListItemSelection(configurations, "Configuration")

# Template is alredy the baseline config.
if selected_configuration == 0:
    saveXML(xml)
    exit(0)


# Select a uri precedence policy
precedence_policies = ["BaseUriPrecedencePolicy", "TextAccessRatioUriPrecedencePolicy", "LogTextToAccessesRatioUriPrecedencePolicy", "TextThresholdAccessesRatioUriPrecedencePolicy"]
selected_precedence_policy = userListItemSelection(precedence_policies, "Uri precedence policy")


# Select a cost assignment policy
cost_ass_policies = ["UnitCostAssignmentPolicy", "AntiCalendarAndQueryCostAssignmentPolicy", "WagCostAssignmentPolicy", "AntiCalendarCostAssignmentPolicy"]
selected_cost_ass_policy = userListItemSelection(cost_ass_policies, "Cost assignment policy")


# Select a queue precedence policy
queue_precedence_policies = ["HighestUriQueuePrecedencePolicy"]
selected_queue_precedence_policy = userListItemSelection(queue_precedence_policies, "Queue precedence policy")


for bean in list(root):

    if bean.get("id") == "candidates":
        # Set configuration
        for bean_property in list(bean):
            if bean_property.get("name") == "crawlItemGrouping":
                bean_property.set("value", str(selected_configuration))
                break

    elif bean.get("id") == "frontier":
        # Set queue precedence policy
        for bean_property in list(bean):
            if bean_property.get("name") == "queuePrecedencePolicy":
                policy_prefix = "org.archive.crawler.frontier.precedence."
                list(bean_property)[0].set("class", policy_prefix + queue_precedence_policies[selected_queue_precedence_policy])
                break

    elif bean.get("id") == "uriPrecedencePolicy":
        # Set uri precedence policy
        policy_prefix = "org.archive.crawler.frontier.precedence."
        bean.set("class", policy_prefix + precedence_policies[selected_precedence_policy])
        if selected_configuration > 0:
            ET.SubElement(bean, "property", { "name": "crawlItemGrouping", "value": str(selected_configuration) })


    elif bean.get("id") == "costAssignmentPolicy":
        # Set cost assignment policy
        policy_prefix = "org.archive.crawler.frontier."
        bean.set("class", policy_prefix + cost_ass_policies[selected_cost_ass_policy])
        
saveXML(xml)