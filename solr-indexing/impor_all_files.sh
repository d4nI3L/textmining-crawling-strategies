#!/bin/bash

for filename in /disk/tmcrawl/warcs/*.warc.gz; do
	[ -e "$filename" ] || continue
	printf "Starting import of $filename...\n"

	printf "Converting to xml...\n"
	java -jar /disk/tmcrawl/warcs_to_solr/jwarcex-solr-standalone-2.0.1.jar $filename output.xml -c
	printf "Convert is done.\n\n"

	printf "Uploading to solr...\n"
	curl -s http://localhost:8983/solr/discovery/update -H "Content-Type: text/xml" --data-binary @output.xml
	curl -s http://localhost:8983/solr/discovery/update?commit=true
	printf "Upload is done.\n"

	printf "Deleting old output..\n"
	rm output.xml

	printf "\nDone.\n\n*******************\n\n\n\n"
done
