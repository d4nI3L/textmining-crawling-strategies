# Solr Setup

## Installing Solr
Download the current solr version with
`wget http://artfiles.org/apache.org/lucene/solr/8.4.0/solr-8.4.0.zip`
and unzip it usign `unzip solr-8.4.0.zip`.

## Setting Solr up
Start solr with `./solr-8.4.0/bin/solr start`. Place your core template in the current directory. Then, create a new discovery core with `./solr-8.4.0/bin/solr create_core -c discovery -d <template_folder>`.

Note that if there have been schema changes, you will need to wipe and create the core, e.g.

```bash
./solr-8.4.0/bin/solr stop
rm -fr ./solr-8.4.0/server/solr/discovery
./solr-8.4.0/bin/solr start
./solr-8.4.0/bin/solr create_core -c discovery -d core_template
```

## Start the import
To import the WARCs into Solr, you first have to extract the relevant data (the document's url, text size and links on the page) and save it as xml. You can do it by usign the tool provided by the Universität Leipzig. To convert the warc to xml, use the following command:
	
```bash
java -jar <path_to_folder_containing_jwarcex>/jwarcex-solr-standalone-2.0.1.jar <warc_file> output.xml -c
```

Then, upload the output.xml to solr and commit the changes using

```bash
curl -s http://localhost:8983/solr/discovery/update -H "Content-Type: text/xml" --data-binary @output.xml
curl -s http://localhost:8983/solr/discovery/update?commit=true
```

	