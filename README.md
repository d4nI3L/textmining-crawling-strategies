# Text Mining PRJ01: Crawling Strategien/Fokus Crawling 

Migrated from git@git.informatik.uni-leipzig.de

# Starting willump 
* got to /willump and run ./start.sh 
* follow the instructions and select the crawl configuration 
* open your browser at localhost:8443/ 
* start crawling 

# Crawling Strategies
* Domain base
* Directory based

## Metrics
* Text/TexAccesses
* Logarithmic TextAccess Scaling
* Threshold for sites with less text 

For details take a look at ./Documents or our code 

# Tools
## Beans generator
A python tool to generate crawler-beans.cxml with a specific grouping configuration (baseline, domain, directory), uri precedence policy, cost assignment policy and queue precedence policy, which the user can choose from interactively.
NOTE: Python 3.6+ is required.

## Beans editor
A php tool for free text editing a crawler-beans.cxml file either in basic or advanced mode. To use the tool, start the start_beans_editor.sh script located inside tools/beans_editor and navigate your browser to http://localhost:8000.
NOTE: PHP is required.


# Changes
* SolrProcessor, SolrProcessorDirectory and SolrProcessorDomain have been unified. You can switch between baseline (0), directory (1) and domain (2) grouping by settings the corresponding value in the crawler beans as the value for the key "crawlItemGrouping" for the "candidates" and "uriPrecedencePolicy" bean
* The startup script now automatically invokes the beans generator python script

