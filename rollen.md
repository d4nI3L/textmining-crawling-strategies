# Teamrollen

* Simon Lämmer: Stellvertreter PL, Entwickler
* Max Zimmer: Entwickler, Technische Leitung
* Lennart Laverenz: Entwickler, Technische Leitung
* Phil Trommer: Verwaltung der Dokumente, Entwickler
* Fabian Thies: Verwaltung der Dokumente, Entwickler
* Daniel Alker: Projektleitung, Entwickler 

## Aufgaben der Rollen
Projektleiter: Planung, Kommunikation mit Betreuern, git (master), coden <br>
Stellvertreter: siehe PL  <br>
Verwaltung: Verwaltung der Dokumente, coden <br>
Technischer Leiter: Fokus auf Programmierung und testen, vorstellung der Technologien, Recherche <br>
