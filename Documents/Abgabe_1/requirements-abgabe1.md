# Abgabe 1 am  22.11.2019 

DEADLINE intern: 20.11.2019
DELINE für Vorbereitung: 16.11.2019

* Name und MtriklNr. der Teilnehmer sowie deren Rolle
* Liste der Aufgaben 
* Lösungsideen 
* Risikoanalyse
* 2 Seiten Umfang

## Was muss geklärt werden 

* Genaue Liste der Aufgaben 
* Lösungsansätze
* Risiken
* Zeiplan

## Aufgaben Liste

* "Plugin" zur Beeinflussung der Priorisierung von Heritrix 
* Aufsetzen eines Testszenarios anhand eines Test-Subsets
* Verbesserung der Crawlingqualität
    * Filterung der Seiten anhand bestimmter Kriterien (Focus Crawling)
    * Priorisierung nach (seltener) Sprachen 
* Messungen und Dokumentation verschidener Ansätze der Veränderung der Crawlingqualität im Testszenario 

## Lösungsansätze
* Max und Lennart beschäftigen sich mit der Technologie
* Webcrawling:
    * Heritrix
    * Wie funktioniert das?
    * Man legt sich verschiedene Profile an, über welche man Crawls startet.
    * Man kann in der crawler-beans.cxml sein Profil konfigurieren, also Links eintragen die verfolgt werden und dort verschiedene Regeln anlegen.
    * Wir vermuten, dass man über die "blockByRegex" oder "allowByRegex" umsetzbar ist, dass wir keine PDFs, Bilder etc. crawlen.
    * Es ist auch möglich eigene Regeln zu machen, da sind wir aber leider auch noch nicht zu einem Erfolg gekommen.
    * Die gecrawlten Seiten werden in WARC files gespeichert, diese kann man entpacken und erhhält die Gecrawlten Inhalte der Seite.
* Suche im "offline" Internet (Subset)
 * Über Solr


## Risiken 
* Phil und Fabi

## Zeitplan 
NOTE: Zeitplan ist nicht final und nicht bindend. Eventuell noch Abstimmung im Team, da die Ressourcen in den Semesterferien noch unklar sind.

* Milstones/Releases
    * 2019-12-10 Release 1: Prototyp - Start von Heritrix und Solr, sowie erstes Crawling mit Plugin ohne Funktionalität
    * 2020-01-16 Release 2: Crawling mit ersten Modifikationen durch eigenes Plugin 
    * 2020-02-20 Release 3: Ausgereifte Funktionalität des Plugins 
    * 2020-03-05 Release 4: Fertige Anwendung und Dokumentation der Messungen

# Nächstes Treffen: 
<b>Vorraussichtlich 21.11.2019 um 17:30 Uhr</b>