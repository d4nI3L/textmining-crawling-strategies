# Protokoll Pitch 13.11.2019
Anwesende: Lennart, Simon, Fabi, Max

## Allgemeines zum Thema
* Crawling Strategien
* Crawling Qualität
  * auf welchen Seiten soll weite gecrawlt werden
* -> "Plugin" um Qualität zu ermitteln
  * Textmenge
  * Textzusammenhänge
  * Große Zusammenhängende Text sollen priorisiert werden
  * vgl. Graphtraversierungsproblem
  * Mustersuche
* Test auf Subset (Websiten die uns zur Verfügung gestellt werden)
* Sparache als Merkmal,wobei besonders seltene Sprachen wichtig sind
  * Betrachtung der Sprache mittels Textextrahierer
  * Focus crawls
* <b>Beeinflussung der Priorisierung von Heretrix</b>
* <b>Testumgebung der Suchmaschine</b>
* <b>Lösung mit Graphproblem</b>
* Erweiterbarkeit (Plugin Chrakter)
* Messungen Dokumentierenng
* Crawl mittels Graphtraversierung steuern

## Technologien
* Java
* Heretrix (siehe github)
  * Sehr flexibel
  * Kann gut konfiguriert werden
* Apache Solr
  * Plugin für warcs
* <b>Springframework</b>
* JUnit
* Script zum starten auf Server

## Projekt und Zeitplan
* Ein bis zwei Seiten
* Deadline 20.03
* Risikoanalyse
  * Testszenario funktioniert?
  * Bereitstellung
  * Performance
  * Datengröße (1-2 Mio Seiten)
  * Crawling Ergebnisse in ein bis zwei Stunden sollten möglich sein

## Präsentation des Protoyps am 20.12 (optimal wegen Betreuer)
* Auch 13.12 möglich
* Solr vllt schon ausetzen
* Initial Setup
* ca. 10 min
